package Entities;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by ydahari on 22/10/2016.
 */
public class Trace {
    public String FullTrace;
    public Activity[] Activities;

    public Trace(Activity[] activities)
    {
        Activities = activities;
        StringJoiner joiner = new StringJoiner(" -> ");
        for(Activity a: activities){
            joiner.add(String.valueOf(a.Name));
        }
        FullTrace = joiner.toString();
    }

    public static Trace Create(char... names)
    {
        Activity[] activities = new Activity[names.length];
        for (int i = 0; i < names.length; i++) {
            activities[i] = new Activity(names[i]);
        }
        return new Trace(activities);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Trace.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Trace other = (Trace) obj;
        return this.FullTrace.equalsIgnoreCase(other.FullTrace);
    }

    @Override
    public int hashCode() {
        return FullTrace.hashCode();
    }
}
