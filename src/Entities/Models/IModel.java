package Entities.Models;

import Entities.Activity;
import Entities.Trace;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by ydahari on 22/10/2016.
 */
public interface IModel {
    String GetName();
    void Train(TraceSet traces);
    boolean Play(Trace trace) throws Exception;
    HashSet<Activity> Activities() throws Exception;
    TraceSet GetTrainingSet();
    boolean TraceExists(Trace trace);
}
