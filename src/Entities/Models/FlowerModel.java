package Entities.Models;

import Entities.Trace;

/**
 * Created by ydahari on 22/10/2016.
 */
public class FlowerModel extends AbstractModel {

    @Override
    protected void TrainSpecific() {
        //Flower model -> do nothing..
    }

    @Override
    protected boolean PlaySpecific(Trace trace) {
        return true;
    }
}
