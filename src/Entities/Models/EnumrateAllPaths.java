package Entities.Models;

import Entities.Trace;

/**
 * Created by ydahari on 22/10/2016.
 */
public class EnumrateAllPaths extends AbstractModel {

    @Override
    protected void TrainSpecific() {
        // tarces are saved
    }

    @Override
    protected boolean PlaySpecific(Trace trace) {
        return TraceExists(trace);
    }
}
