package Entities.Models;

import Entities.Activity;
import Entities.Trace;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by ydahari on 22/10/2016.
 */
public abstract class AbstractModel implements IModel {
    private boolean _trained;
    private HashSet<Activity> _activities;
    private HashSet<Trace> _traces;
    private String _name;

    protected TraceSet _trainSet;

    public AbstractModel(String name)
    {
        _trained = false;
        _trainSet = null;
        _activities = new HashSet<Activity>();
        _traces = new HashSet<Trace>();
        _name = name;
    }

    public AbstractModel()
    {
        _trained = false;
        _trainSet = null;
        _activities = new HashSet<Activity>();
        _traces = new HashSet<Trace>();
        _name = getClass().getSimpleName();
    }

    @Override
    public String GetName() {
        return _name;
    }

    @Override
    public void Train(TraceSet traces) {
        _trainSet = traces;
        for (Trace t: traces.Traces) {
            _traces.add(t);
            for (Activity a: t.Activities) {
                _activities.add(a);
            }
        }

        try {
			TrainSpecific();
			_trained = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			_trained = false;
		}        
    }

    @Override
    public boolean Play(Trace trace) throws Exception {
        if (!_trained)
        {
            throw new Exception("Model not trained yet");
        }
        return PlaySpecific(trace);
    }

    @Override
    public HashSet<Activity> Activities() throws Exception {
        if (!_trained)
        {
            throw new Exception("Model not trained yet");
        }

        return _activities;
    }

    @Override
    public TraceSet GetTrainingSet() {
        return _trainSet;
    }

    @Override
    public boolean TraceExists(Trace trace)
    {
        return _traces.contains(trace);
    }

    protected abstract void TrainSpecific() throws Exception;

    protected abstract boolean PlaySpecific(Trace trace);

}
