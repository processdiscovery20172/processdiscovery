package Entities.Models;

import Entities.Trace;

/**
 * Created by ydahari on 23/10/2016.
 */
public class TraceSet {
    public Trace[] Traces;

    public TraceSet(Trace... traces)
    {
        Traces = traces;
    }

    public static TraceSet Create(String... traces)
    {
        Trace[] tr = new Trace[traces.length];
        for (int i = 0; i < traces.length; i++) {
            String str = traces[i];
            Trace t = Trace.Create(str.toCharArray());
            tr[i] = t;
        }

        return new TraceSet(tr);
    }
}
