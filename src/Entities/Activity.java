package Entities;

/**
 * Created by ydahari on 22/10/2016.
 */
public class Activity {
    public char Name;

    public Activity(char name) throws IllegalArgumentException {
        Name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Activity.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Activity other = (Activity) obj;
        return this.Name == other.Name;
    }

    @Override
    public int hashCode() {
        return String.valueOf(Name).hashCode();
    }
}
