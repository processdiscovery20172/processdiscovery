package Entities;


import java.util.*;

/**
 * Created by ydahari on 22/10/2016.
 */

//TODO: implements Iterable<Trace> ?
public class Space {

    private Activity[] _activities;
    private int _length;

    public static List<Trace> GetAll(int length, char[] chars) {
        final double NUMBER_OF_PERMUTATIONS = Math.pow(chars.length, length);

        List<Trace> words = new ArrayList<>(Double.valueOf(
                NUMBER_OF_PERMUTATIONS).intValue());

        char[] temp = new char[length];
        Arrays.fill(temp, chars[0]);

        for (int i = 0; i < NUMBER_OF_PERMUTATIONS; i++) {
            int n = i;
            for (int k = 0; k < length; k++) {
                temp[k] = chars[n % chars.length];
                n /= chars.length;
            }
            String word = String.valueOf(temp);
            Trace trace = Trace.Create(word.toCharArray());
            words.add(trace);
            //words.add(String.valueOf(temp));
        }
        return words;
    }

    public Space(HashSet<Activity> activities, int length) {
        _activities = activities.toArray(new Activity[activities.size()]);
        _length = length;
    }

    public List<Trace> GetAll()
    {
        char[] chars = new char[_activities.length];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = _activities[i].Name;
        }

        return GetAll(_length, chars);
    }
    /*
    @Override
    public Iterator<Trace> iterator() {
        return new Iterator<Trace>() {

            int[] indexes = new int[_activities.length];
            int maxIndex = _length - 1;

            private void IncrementOne()
            {
                for (int i = 0; i < indexes.length; i++) {
                    if (indexes[i] < maxIndex){
                        indexes[i] = indexes[i] + 1;
                        return;
                    }
                }
            }


            @Override
            public boolean hasNext()
            {
                for (int i: indexes) {
                    if (i < maxIndex){
                        return true;
                    }
                }

                return false;
            }

            @Override
            public Trace next()
            {
                if (hasNext())
                {
                    Activity[] activities = new Activity[indexes.length];
                    for (int i = 0; i < activities.length; i++) {
                        activities[i] = _activities[indexes[i]];

                    }
                    Trace trace = new Trace(activities);

                    IncrementOne();
                    return trace;
                }
                throw new NoSuchElementException("No more traces available");
            }

            @Override
            public void remove()
            {
                throw new UnsupportedOperationException("Removals are not supported");
            }};
    }
    */
}
