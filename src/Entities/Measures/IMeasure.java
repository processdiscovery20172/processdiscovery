package Entities.Measures;

import Entities.Models.IModel;
import Entities.Models.TraceSet;
import Entities.Trace;

/**
 * Created by ydahari on 22/10/2016.
 */
public interface IMeasure {
    double GetValue(IModel model, TraceSet traces) throws Exception;
    String GetName();
}
