package Entities.Measures;

import Entities.Models.IModel;
import Entities.Models.TraceSet;
import Entities.Trace;

import java.util.Arrays;

/**
 * Created by ydahari on 22/10/2016.
 */
public class Fitness implements IMeasure {


    public double GetValue(IModel model, TraceSet traces) throws Exception {
        if (traces == null)
        {
            traces = model.GetTrainingSet();
        }

        double canPlay = 0;
        for (Trace t: traces.Traces) {
            if (model.Play(t))
            {
                canPlay++;
            }
        }

        return canPlay / (double)traces.Traces.length;
    }

    @Override
    public String GetName() {
        return "Fitness";
    }
}
