package Entities.Measures;

import Entities.Activity;
import Entities.Models.IModel;
import Entities.Models.TraceSet;
import Entities.Space;
import Entities.Trace;

import java.util.HashSet;
import java.util.Iterator;


/**
 * Created by ydahari on 22/10/2016.
 */
public class Precision implements IMeasure {

    @Override
    public double GetValue(IModel model, TraceSet traces) throws Exception {

        //Note: traces isn't being used in this measure.


        HashSet<Activity> activities = model.Activities();
        Space sp = new Space(activities, activities.size());

        double canPlay = 0;
        double canPlayAndInTrain = 0;
        //for (Iterator<Trace> spaceIter = sp.iterator(); spaceIter.hasNext();){
        //    Trace trace = spaceIter.next();

        for (Trace trace: sp.GetAll()) {
            if (model.Play(trace))
            {
                canPlay++;

                if (model.TraceExists(trace))
                {
                    canPlayAndInTrain++;
                }
            }


        }

        return canPlayAndInTrain /canPlay;
    }

    @Override
    public String GetName() {
        return "Precision";
    }
}
