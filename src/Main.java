import Entities.Measures.Fitness;
import Entities.Measures.Precision;
import Entities.Models.EnumrateAllPaths;
import Entities.Models.FlowerModel;
import Entities.Models.IModel;
import Entities.Models.InductiveMiner;
import Entities.Models.TraceSet;
import Entities.Trace;


public class Main {

    public static void main(String[] args) {
        try {
            //Choose Sets
            TraceSet trainSet = TraceSet.Create("ABC", "CBA", "AAB");
            TraceSet testSet = TraceSet.Create("ABC", "CBA", "AAB", "CCB");

            //Choose model
            IModel model = new InductiveMiner();
            //IModel model = new EnumrateAllPaths();

            //Train the model
            model.Train(trainSet);

            //Choose fitness measure
            Fitness fitnessMeasure = new Fitness();
            Precision precisionMeasure = new Precision();

            double fitness  = fitnessMeasure.GetValue(model, testSet);
            double precision = precisionMeasure.GetValue(model, null);


            System.out.printf("Model %s, measure: %s, value: %.3f", model.GetName(), fitnessMeasure.GetName(), fitness);
            System.out.println();
            System.out.printf("Model %s, measure: %s, value: %.3f", model.GetName(), precisionMeasure.GetName(), precision);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
